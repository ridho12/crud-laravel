
@extends('adminLTE.master')

@section('content')
<!-- <a href="/pertanyaan/create" class="btn btn-primary">Tambah</a> -->
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Judul</th>
                <th scope="col">Isi</th>
                <th scope="col" style="display: inline">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($pertanyaan as $key=>$per)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$per->judul}</td>
                        <td>{{$per->isi}}</td>
                        <!-- <td>
                            <form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
                                <a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-info">Show</a>
                                <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-primary">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td> -->
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
        @endsection